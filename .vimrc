" Source a global configuration file if available
if filereadable("/etc/vim/gvimrc.local")
  source /etc/vim/gvimrc.local
endif

" Настройка для плагина Pathogen
call pathogen#infect()

syntax enable " Включаем цветовую подсветку синтаксиса
set nocompatible " отключить совместимость с Vi

set nobackup " не создавать резервные копии файлов (например, файлов с ~ на конце: myfile.txt~)
set noundofile
set swapfile

set noautowrite " отменить автоматическое сохранение (например командой :write)
set number

set ruler " Линейка позволяет всегда показывать текущее положение курсора в нижнем правом углу экрана Vim
set showcmd " Показывать незавершенную команду в правом нижнем углу экрана Vim, слева от линейки
set cmdheight=1 " Указываем количество строк, используемых для вывода сообщений
set history=50 " Хранить в истории 50 команд и 50 шаблонов для поиска
set wildmenu
set wcm=<Tab>
set whichwrap=b,s,<,>,[,] " Позволяет кнопкам <BS>, <Left>(в обычном режиме и в режиме вставки), когда она используется в первой позиции строки, 
                          " перемещать курсор в конец предыдущей строки
                          " Кнопка <Space> (пробел), <Rigtht>(в обычном режиме и в режиме вставки) перемещает курсор от конца строки к началу следующей

set foldmethod=marker
" set formatoptions -= ro " Отключение автоматического комментирования

filetype plugin indent on

" Настройка отступов
" ==================
set tabstop=2
set shiftwidth=2
set expandtab
set smarttab
set cindent
" ==================

" Настройка кодировок
" ===================
set fileencodings=utf-8,cp1251,cp866,koi8-r,koi8-u
set fileformats=unix,dos,mac
set encoding=utf-8
" ===================

" Настройка раскладок клавиатуры
" ==============================
set iskeyword=@,a-z,A-Z,48-57,_,128-175,192-255
" Настраиваем переключение раскладок клавиатуры по <C-^>
set keymap=russian-jcukenwin
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz
" Раскладка по умолчанию - английская
set iminsert=0
set imsearch=0
" ==============================

" Настройка поиска (search)
" =========================
set incsearch   " При поиске перескакивать на найденный текст в процессе набора строки
set hlsearch    " Включаем подсветку выражения, которое ищется в тексте
set ignorecase  " Игнорировать регистр букв при поиске
" =========================
"
" Настройка курсора
" =================
set cursorline
set cursorcolumn
" highlight CursorLine guibg=White
" highlight CursorColumn guibg=White
highlight CursorColumn ctermbg=White

hi clear CursorLine
augroup CLClear
    autocmd! ColorScheme * hi clear CursorLine
augroup END

hi CursorLineNR cterm=bold
augroup CLNRSet
    autocmd! ColorScheme * hi CursorLineNR cterm=bold
augroup END
" =================
"

let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set termguicolors
set background=light
colorscheme solarized8

" Установка более информативной командной строки (status line)
" ============================================================
set statusline=%F\ [FORMAT=%{&ff}]\ FILE=%{&fileencoding}\ ENC=%{&encoding}\ [TYPE=%Y]\ [ASCII=\%04.4b]\ [HEX=\%04.4B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]
set laststatus=2
" ============================================================

" Команды смены сочетания клавиш (mapping)
" ========================================
map <S-Enter> O<Esc>
map <cr> o<Esc>

imap <C-Space> <C-x><C-o>

vmap ,x :!tidy -q -i -utf8 --show-errors 0 --wrap 0<CR>
"vmap ,x :!tidy -q -i -utf8 --show-errors 0 --wrap 0 --indent yes<CR>

map [[ ?{<CR>w99[{
map ][ /}<CR>b99]}
map ]] j0[[%/{<CR>
map [] k$][%?}<CR>
" ========================================

" Настройки консольного vim
if !has("gui_running")
  set ttymouse=xterm2
  set mouse=a
  set clipboard=unnamedplus
endif


" Сортировать
" ===========
autocmd BufNewFile * silent! 0r $VIM/vimfiles/templates/%:e.tpl
nnoremap <c-j> /<+.\{-1,}+><cr>c/+>/e<cr>
inoremap <c-j> <ESC>/<+.\{-1,}+><cr>c/+>/e<cr>

" Устанавливаем проверку орфографии
" чтобы проверить нужные варианты в обычном режиме набираем z=
" set spell
" set spelllang=en,ru
" ===========


" ##### Настройки плагинов #####

" Настройки Tagbar - плагина просмотра структуры исходного кода 
" =============================================================
":TagbarGetTypeConfig perl
let g:tagbar_type_perl = {
  \ 'ctagstype' : 'perl',
  \ 'kinds' : [
    \ 'c:constants',
    \ 'f:formats',
    \ 'l:labels',
    \ 'p:packages',
    \ 's:subroutines',
    \ 'd:subroutine declarations [off]',
    \ 'w:roles',
    \ 'e:extends',
    \ 'u:uses',
    \ 'r:requires',
    \ 't:attributes',
    \ 'a:aliases',
    \ 'h:helpers',
    \ 'o:ours',
    \ 'i:Plain Old Documentation' 
  \]
\ }

nmap <F3> :TagbarToggle<cr>
imap <F3> <esc>:TagbarToggle<cr>i<right>

" настраиваем tagbar так, чтобы при выборе метода, класса и т.п. плагин не
" только переносил курсор на соответствующее объявление, но и закрывал свое
" окно
"let g:tagbar_autoclose = 1
" =============================================================

" Настройка NERDTree
" ==================
nmap <F4> :NERDTreeToggle<cr>
imap <F4> <esc>:NERDTreeToggle<cr>i<right>
" let NERDTreeSortOrder=['*', '\/$']
" ==================

" Настройка UltiSnips
" ==================
" UltiSnips triggers
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:UltiSnipsSnippetsDir=$HOME.'/.vim/bundle/vim-snippets/UltiSnips'
let g:UltiSnipsSnippetDirectories=$HOME.'/.vim/bundle/vim-snippets/UltiSnips'
" ==================

" Настройка eregex
" ================
" '/' will use :M/ to search. ',/' will use the original '/'.
nnoremap / :M/
nnoremap ? :M?
nnoremap ,/ /
nnoremap ,? ?
let eregex_replacement=3
" ================

" Настройка Syntastic
" ===================
let g:syntastic_enable_signs = 1
let g:syntastic_check_on_open = 1
let g:syntastic_enable_perl_checker = 1
" let g:syntastic_perl_lib_path = ['/path/to/lib', '/path/to/lib']

" C++11
let g:syntastic_cpp_compiler = 'g++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++'

" let g:syntastic_perl_checkers = ['perl', 'perlcritic', 'podchecker']
let g:syntastic_perl_checkers = ['perl']
" ===================

" Настройка delimitMate
" =====================
" let delimitMate_expand_cr = 1
" let delimitMate_expand_space = 1
" let delimitMate_jump_expansion = 1

" imap <silent> <buffer> <Tab> <Plug>delimitMateJumpMany<CR>
" imap <silent> <buffer> <C-G>g <Plug>delimitMateJumpMany

" inoremap <buffer> <Plug>delimitMateJumpMany <C-R>=len(b:_l_delimitMate_buffer) ? delimitMate#Finish(0) : delimitMate#JumpMany()<CR>
" " imap <silent> <buffer> <Tab> <Plug>delimitMateJumpMany
" " imap <buffer> <Tab> <Plug>delimitMateJumpMany
" =====================

" Настройка auto-pairs
" ====================
let g:AutoPairsFlyMode = 1
" ====================



" Настройка Airline
" =================
" let g:airline#extensions#tabline#enabled = 1
" let g:airline_powerline_fonts = 1
" let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
" =================
"
" Настройка viewdoc
" =================
let g:viewdoc_open='topleft new'
let mapleader = ","
" =================

" Настройка AsyncRun
" ==================
" a help function to toggle quickfix window in an efficient way (F9 to toggle): 
noremap <F9> :call asyncrun#quickfix_toggle(8)<cr>
" ==================

" Настройка Surround
" ==================
map tp yss<p><cr>
" ==================

" Настройка Undotree
" ==================
nnoremap    <F5>    :UndotreeToggle<cr>
" ==================

" Настройка mojo.vim
" ==================
let mojo_highlight_data = 1
" ==================

" Настройка dbext
" ===============
let g:dbext_default_type = 'MYSQL'
let g:dbext_default_user   = 'root'
let g:dbext_default_passwd = ''
let g:dbext_default_host = '127.0.0.1'
let g:dbext_default_port = '3306'

let g:dbext_default_profile_mySQLServer = 'type=MYSQL:dbname=dsp_admin:user=root:passwd=:host=127.0.0.1'
let g:dbext_default_profile = 'mySQLServer'

":exec 'DBSetOption type=MYSQL:dbname=dsp_admin:user=dsp_admin:passwd=Qrtc7_83TLh:host=localhost'
"DBSetOption type=MYSQL

"DBSetOption passwd=Qrtc7_83TLh
"DBSetOption host=localhost
"DBSetOption dbname=dsp_admin
" ===============
" ##############################

" ##### Настройки, специфичные для Perl #####
" To use gf with perl
" ===================
set path+=/usr/lib/perl5,/usr/local/share/perl5,$PWD/.cpan
set path+=/path/to/lib,/path/to/lib
set path+=/path/to/lib
autocmd BufRead *.pl,*.pm set include=^use
autocmd BufRead *.pl,*.pm set isfname+=: " включаем символ ':' в имена файлов и путевые имена
autocmd BufRead *.pl,*.pm set suffixesadd+=.pm " Добавляем расширение '.pm' для поиска файла командой gf
autocmd BufRead *.pl,*.pm set includeexpr=substitute(substitute(v:fname,'::','/','g'),'$','.pm','')
" ===================

" Fold perl POD
au FileType perl set foldmethod=syntax | syn region PODHead start=/^=head[123]/ end=/^=cut/ fold | syn region PODPod start=/^=\(pod\|func\)/ end=/^=cut/ fold

au BufRead,BufNewFile *.tt2 set filetype=tt2

" Настройка плагина perl_search_lib
" =================================
"let g:perl_search_lib='/path/to/lib,/path/to/lib'
" =================================

" Сортировать
" set includeexpr=substitute(v:fname,'::','/','g')
" set path+=/CompanyCode/*, " directory containing work code
" autocmd BufRead *.p? set include=^use
" autocmd BufRead *.pl set includeexpr=substitute(v:fname,'\\(.*\\)','\\1.pm','i')

" verbose au FileReadCmd set includeexpr=GetGrwFNName(v:fname)

" autocmd BufRead *.pl set includeexpr=GetGrwFNName(v:fname)
" autocmd BufRead *.pl set includeexpr=GetModuleName(v:fname)
" set includeexpr=substitute(v:fname,'\\(.*\\)','\\1.pm','i')

"
" function! s:GetModuleName(name)
"   let l:output = a:name
"   if a:name =~ "\.pm"
"     let output = substitute(v:fname,'::','/','g')
"   endif
"   return l:output
" endfunction
" 
" function! GetGrwFNName(name)
"   let l:output = s:GetModuleName(a:name)
"   return l:output
" endfunction
" ###########################################

" ##### Настройки, специфичные для CoffeeScript и Stylus #####
" Compiling CoffeeScript and Stylus with Vim
" Tip: Use Ctrl+L to clear any compiler errors and redraw the screen or delete the silent option from the Vim autocmd.
autocmd BufWritePost,FileWritePost *.coffee silent !coffee -c <afile>
autocmd BufWritePost,FileWritePost *.styl,*.stylus silent !stylus <afile> >/dev/null
" ############################################################

" Filetype sql settings
let g:sql_type_default = 'mysql'

" https://github.com/vim-scripts/Colour-Sampler-Pack
" Еще интересные темы: nuvola, sienna, tolerable, soso, chela_light
" colorscheme mayansmoke

" Передвижение табов в определенном порядке
" Next tab: gt
" Prior tab: gT
" Numbered tab: nnngt
" ------------------

if filereadable("~/.custom-settings.vim")
  source ~/.custom-settings.vim
endif

" if !exists('g:airline_symbols')
"     let g:airline_symbols = {}
" endif

" " unicode symbols
" let g:airline_left_sep = '»'
" let g:airline_left_sep = '▶'
" let g:airline_right_sep = '«'
" let g:airline_right_sep = '◀'
" let g:airline_symbols.linenr = '␊'
" let g:airline_symbols.linenr = '␤'
" let g:airline_symbols.linenr = '¶'
" let g:airline_symbols.branch = '⎇'
" let g:airline_symbols.paste = 'ρ'
" let g:airline_symbols.paste = 'Þ'
" let g:airline_symbols.paste = '∥'
" let g:airline_symbols.whitespace = 'Ξ'

" " airline symbols
" let g:airline_left_sep = ''
" let g:airline_left_alt_sep = ''
" let g:airline_right_sep = ''
" let g:airline_right_alt_sep = ''
" let g:airline_symbols.branch = ''
" let g:airline_symbols.readonly = ''
" let g:airline_symbols.linenr = ''

" set guifont=DejaVu\ Sans:s12

python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup
set laststatus=2 " Always display the statusline in all windows
set showtabline=2 " Always display the tabline, even if there is only one tab
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
set t_Co=256
