### Алиасы для git-а ###
alias gst='git status'
alias ga='git add'
alias gc='git commit -m'
alias gp='git pull && git push'
alias gull='git pull'
alias gush='git push'
alias gb='git branch'
alias gco='git checkout'
alias gd='git diff'
# Для работы алиаса должен быть добавлен git-алиас в конфиге ~/.gitconfig
# [alias]
# 	hist = log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short
alias gh='git hist'
########################

alias psc='ps xawf -eo pid,user,cgroup,args'
alias pmver="perl -le '\$m = shift; eval qq(require \$m) \
    or die qq(module \"\$m\" is not installed\\n); \
    print \$m->VERSION'"

alias now='date +"%T"'
alias path='echo -e ${PATH//:/\\n}'

alias ports='ss -tulanp'

# get web server headers #
alias header='curl -I'
# find out if remote server supports gzip / mod_deflate or not #
alias headerc='curl -I --compress'

# confirmation #
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'
alias mkdir='mkdir -pv'
# do not delete / or prompt if deleting more than 3 files at a time #
alias rm='rm -I --preserve-root'
# Stop after sending count ECHO_REQUEST packets #
alias ping='ping -c 5'
 
# Parenting changing perms on / #
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'

alias ...='cd .. && cd ..'
alias lsA='ls -lhA'

## Get server cpu info ##
alias cpuinfo='lscpu'

# TODO: добавить вывод версии mysql-сервера - \v
alias colormysql=$(echo -e 'mysql --prompt="\x1B[31m\\u\x1B[34m@\x1B[32m\\h\x1B[0m:\x1B[36m\\d>\x1B[0m "')
