# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# bash-powerline
if [ -f ~/.bash-powerline.sh ]; then
  . ~/.bash-powerline.sh
fi

# custom configs
if [ -f ~/.bash-custom.sh ]; then
  . ~/.bash-custom.sh
fi

if [ -f ~/.bash_aliases ]; then
  . ~/.bash_aliases
fi

# Писать команду в историю bash сразу - помогает при работе с несколькими шеллами
shopt -s histappend
PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
# Не писать в историю подряд идущие строки-дубликаты и игнорировать команды, начинающиеся с пробела
export HISTCONTROL="ignorespace:ignoredups"
# Запретить логирование "неинформативных" команд (в данном случае &, ls, bg, fg)
export HISTIGNORE="&:ls:[bf]g:exit"
HISTSIZE=5000
HISTFILESIZE=10000
# Выводить команду из истории вместо выполнения. Позволяет предварительно подредактировать команду перед выполнением
shopt -s histverify

# Эвристическое исправление ошибок имен директорий для команды cd
shopt -s cdspell
# Эвристическое исправление ошибок имен директорий при автодополнении
shopt -s dirspell
# Добавляем возможность переходить в каталог без указания команды cd
shopt -s autocd

# Если опция выставлена - glob ** - становится рекурсивным. 
# То есть ведётся поиск соответствия всем файлам и 0 или больше поддиректориям. 
# Если указать / в конце шаблона, то сопоставляются только директории
shopt -s globstar

# Запрещаем выход из консоли если в ней есть выполняющиеся задания.
shopt -s checkjobs

# (since bash 4.3-alpha)
# Interprets [a-d] as [abcd]. To match a literal -, include it as first or last character. 
shopt -s globasciiranges

# Не разрывать многострочные команды
shopt -s cmdhist

# предотвращает перезаписть файлов
set -o noclobber

if [[ -s "$HOME/.bashrc.d/autocomplete.sh" ]]; then
    source "$HOME/.bashrc.d/autocomplete.sh" 
    bind -x '"\t" : autocomplete_wrapper'
fi

# Bash::Completion::Plugins::cpanm
# https://metacpan.org/pod/Bash::Completion::Plugins::cpanm
# source setup-bash-complete

[[ -e ~/.phpbrew/bashrc ]] && source ~/.phpbrew/bashrc
