# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

if [ -f ~/.bash_profile_custom.sh ]; then
	. ~/.bash_profile_custom.sh
fi

# User specific environment and startup programs

# PS1='\[\e[0;35m\]\t \[\e[4;33m\]\u@\h\[\e[0;34m\] \w\n\[\e[1;34m\]\$\[\e[00m\] '
#PS1='\[\e[1;35m\]\t \[\e[4;33m\]\u@\h\[\e[1;36m\] $PWD\n\[\e[1;34m\]\$\[\e[00m\] '

export PATH=$HOME/scripts/:/opt/rakudo-pkg/bin:$HOME/perl5/bin:$PATH:$HOME/.local/bin:$HOME/bin

export PERL_LOCAL_LIB_ROOT="$PERL_LOCAL_LIB_ROOT:$HOME/perl5"
export PERL_MB_OPT="--install_base $HOME/perl5"
export PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"
export PERL5LIB="/usr/lib64/perl5/vendor_perl:$HOME/perl5/lib/perl5:$PERL5LIB"

export NODE_PATH=/usr/lib/node_modules:$NODE_PATH
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.0.x86_64"
export FCEDIT="/usr/bin/vim"
export LESSCHARSET=utf-8

# if [ -f $HOME/.rakudobrew/bin/rakudobrew ]; then
#   eval "$($HOME/.rakudobrew/bin/rakudobrew init -)"
# fi

PATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PATH}))')"

export PHPBREW_SET_PROMPT=1
export PHPBREW_RC_ENABLE=1
