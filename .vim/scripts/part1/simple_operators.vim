" Простые арифметические операции
let a = 1
echo "a =" a

let b = a + 1
echo "b =" b

let b = a - 2
echo "b =" b

let c = a * 3
echo "c =" c

let d = a / 4
echo "d =" d

let e = a / 4.0
echo "e =" e

unlet a b c d e

" Объединение строк
let foo = "foo "
let bar = "bar"
let spam = foo . bar
echo spam

unlet foo bar spam

" Использование операторов +=, -= и .=
let foo = 5
let bar = "Abyrvalg"

let foo += 15
" foo равен 20
echo foo

let bar .= " rulezzz"
" bar равен 'Abrvalg rulezzz'
echo bar

unlet foo bar

" Автоматическое преобразование чисел в строку
let foo = "Abyrvalg "
let bar = 123

let spam = foo . bar
echo spam

unlet foo bar spam

" Автоматическое преобразование строки в число
let foo = "5 "
let bar = 123

let spam1 = foo + bar
echo "spam1 =" spam1 "type = " type(spam1)

let spam2 = foo . bar
echo "spam2 =" spam2 "type = " type(spam2)

unlet foo bar spam1 spam2
