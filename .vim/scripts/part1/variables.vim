" Целоео число
let a = 1

" Число с плавающей точкой
let b = a + 1.0

" Строка
let c = "abyrvalg"

" Тоже строка
let d = 'abyrvalg'

" Список
let e = [1, 2, 3]

" Словарь
let f = {"foo": 1, "bar": 2, "qqq": [1, 2, 3]}

echo "a =" string(a) "type =" type(a)
echo "b =" string(b) "type =" type(b)
echo "c =" string(c) "type =" type(c)
echo "d =" string(d) "type =" type(d)
echo "e =" string(e) "type =" type(e)
echo "f =" string(f) "type =" type(f)

unlet a b c d e f

echo "Hello" | " А это комментарий
