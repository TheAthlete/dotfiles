let s:foo = 101
echo s:foo
unlet s:foo

" В разных областях видимости разные переменные могут иметь одинаковые имена
let foo = 202
let s:foo = 303

echo foo
echo s:foo

unlet foo s:foo
