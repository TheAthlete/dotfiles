" Скрипт добавляет к концу открытого файла количество содержащихся в нем строк
let s:count = line("$")

let s:line1 = "------------"
let s:line2 = printf("Lines count: %d", s:count)

call append(s:count, [s:line1, s:line2])

unlet s:count s:line1 s:line2
