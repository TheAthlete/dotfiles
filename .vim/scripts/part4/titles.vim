" Скрипт автоматически создает оглавление для статьи

" Регулярное выражение для нахождения якоря
let s:pattern_anchor = '\m^\[\[#\([a-zA-Z0-9_]*\)\]\]'

" Регулярное выражение для нахождения заголовка
let s:pattern_title = '\m^!!\s*\(.*\)$'

" Количество строк в файле
let s:count = line("$")

" Сюда будут добавляться строки оглавления
let s:result = []

" Проходимся по всем строкам в буфере
for n in range(1, s:count)
  " Получим номер строки по ее номеру
  " Строки в буфере нумеруются с 1
  let s:currline = getline(n)

  " Для каждой строки проверим соответствует ли она шаблону с якорем
  let s:anchor = matchlist(s:currline, s:pattern_anchor)

  if len(s:anchor) != 0
    " Если строка соответствует, то список не будет пустым
    
    " Теперь проверим соответствует ли следующая строка шаблону заголовка
    let s:nextline = getline(n+1)
    let s:title = matchlist(s:nextline, s:pattern_title)

    if len(s:title) != 0
      " Если и заголовок найден, создадим строку оглавления
      let s:resline = printf("* [[#%s | %s]]", s:anchor[1], s:title[1])
      call add(s:result, s:resline)
    endif
  endif

endfor

" Получить положение курсора в виде списка: [номер буфера, номер строки, номер
" столбца, параметр при использовании опции virtualedit]
let s:cursor = getpos(".")

" Вставим полученное оглавление в ту строку, где сейчас стоит курсор
call append(s:cursor[1], s:result)

" Удалим все переменные
unlet s:pattern_anchor s:pattern_title s:count
unlet! s:resline s:nextline s:title s:anchor s:currline
