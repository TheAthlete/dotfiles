let foo = [1, 2, 3, 4, 5]
let bar = ["abyrvalg", "spam", "bla-bla-bla"]
let spam = [1, 3.0, "Hello, Vim", [3, 4, 5]]

echo foo
echo bar
echo spam

unlet foo bar spam

let foo = [0, 1, 2, 3, 4]
echo foo[0]
echo foo [0]

unlet foo

" Срезы
let foo = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
echo foo
echo foo[2: 6]

echo foo[0: 2]
echo foo[: 2]

echo foo[5: -1]
echo foo[5:]

echo foo[0: -1]
echo foo[:]

unlet foo

" Распаковка списка
let foo = [0, 1, 2, 3, 4]
let [foo_0, foo_1, foo_2, foo_3, foo_4] = foo
echo foo_0 foo_1 foo_2 foo_3 foo_4

unlet foo_0 foo_1 foo_2 foo_3 foo_4

let [foo_0, foo_1; tail] = foo
echo foo_0 foo_1 tail

unlet foo_0 foo_1 tail foo

" Оператор for
let foo = [0, 1, 2, 3, 4]
for bar in foo
  echo bar
endfor

unlet bar foo
