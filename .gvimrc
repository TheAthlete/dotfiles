" Меню выбора/изменения кодировки текста (utf-8, cp1251, cp866, koi8-r, koi8-u)
menu ChangeFileEncoding.windows-1251 :set fileencoding=cp1251<CR>
menu ChangeFileEncoding.utf-8 :set fileencoding=utf8<CR>
menu ChangeFileEncoding.cp866 :set fileencoding=cp866<CR>
menu ChangeFileEncoding.koi8-r :set fileencoding=koi8-r<CR>
menu ChangeFileEncoding.koi8-u :set fileencoding=koi8-u<CR>
map <F8> :emenu ChangeFileEncoding.<TAB>
" ==================

" ==================
set guioptions+=b " добавляет нижнюю горизонтальную полосу прокрутки, что удобно в случае текста с длинными строчками
set guioptions-=T " убирает панель инструментов
set guioptions-=m " по умолчанию меню скрыто
set nowrap      " убираем перенос строк, иначе опция guioptions+=b бессмысленна
set columns=157 " количество колонок текста в окне для разрешения экрана 1280x1024
set lines=30    " задаем количество строк текста в окне.

" showing long lines
set sidescroll=5
set listchars+=precedes:<,extends:>

" Настройка меню 
" ------------------
" Горячие клавиши скрытия/отображения меню
function MyToggleMenu()
  let old_guioptions = &guioptions
  " Если меню в данный момент видимо
  if stridx(old_guioptions, 'm') != -1
    set guioptions-=m
  else
    set guioptions+=m
  endif
endfunction

cmap <F12> <ESC>:call MyToggleMenu()<CR>
imap <F12> <ESC>:call MyToggleMenu()<CR>
nmap <F12> :call MyToggleMenu()<CR>
vmap <F12> <ESC>:call MyToggleMenu()<CR>
" ------------------

set background=light
colorscheme solarized

" пытаемся занять максимально большое пространство на экране.
" Также можно посмотреть плагин https://github.com/itmammoth/maximize.vim
set lines=999 columns=999

highlight CursorColumn guibg=White

hi clear CursorLine
augroup CLClear
    autocmd! ColorScheme * hi clear CursorLine
augroup END

hi CursorLineNR gui=bold
augroup CLNRSet
    autocmd! ColorScheme * hi CursorLineNR gui=bold
augroup END
